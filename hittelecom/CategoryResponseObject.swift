//
//  CategoryResponseObject.swift
//  hittelecom
//
//  Created by ifau on 23/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class CategoryResponseObject: NSObject
{
    var categories: [Category]!
    var success: Bool!

    init(fromDictionary dictionary: NSDictionary)
    {
        categories = [Category]()
        if let categoriesArray = dictionary["Categories"] as? [NSDictionary]
        {
            for dic in categoriesArray
            {
                let value = Category(fromDictionary: dic)
                categories.append(value)
            }
        }
        success = dictionary["Success"] as? Bool
    }
}

class Category: NSObject
{
    var haveChildren: Bool!
    var iD: String!
    var name: String!

    init(fromDictionary dictionary: NSDictionary)
    {
        haveChildren = dictionary["HaveChildren"] as? Bool
        iD = dictionary["ID"] as? String
        name = dictionary["Name"] as? String
    }
}