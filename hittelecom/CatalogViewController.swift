//
//  CatalogViewController.swift
//  hittelecom
//
//  Created by ifau on 23/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CatalogViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, filterViewControllerDelegate
{
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    
    internal var categoryID: String?
    internal var categoryName: String?
    
    fileprivate var categories: [Category] = []
    fileprivate var products: [Product] = []
    
    //
    fileprivate var isSortingEnable = false
    fileprivate var inAscendingSortingOrder = false
    fileprivate var inDescendingSortingOrder = false
    fileprivate var minPriceForSorting: Int?
    fileprivate var maxPriceForSorting: Int?
    fileprivate var brandsForSorting: [String]?
    fileprivate var sortedProducts: [Product]?
    //
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        if categoryName == nil
        {
            let searchButton = UIButton(type: .custom)
            searchButton.addTarget(self, action: #selector(CatalogViewController.searchButtonPressed(_:)), for: .touchUpInside)
            searchButton.setImage(UIImage(named: "IconSearch"), for: UIControlState())
            searchButton.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, -15);
            searchButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            
            let item = navigationItem.rightBarButtonItem!
            let item2 = UIBarButtonItem(customView: searchButton)
            navigationItem.rightBarButtonItems = [item, item2]
            
            navigationItem.title = "Каталог"
        }
        else
        {
            let filterButton = UIButton(type: .custom)
            filterButton.addTarget(self, action: #selector(CatalogViewController.filtersButtonPressed(_:)), for: .touchUpInside)
            filterButton.setImage(UIImage(named: "IconFilter"), for: UIControlState())
            filterButton.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, -15);
            filterButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            
            let item = navigationItem.rightBarButtonItem!
            let item2 = UIBarButtonItem(customView: filterButton)
            navigationItem.rightBarButtonItems = [item, item2]
            
            navigationItem.title = categoryName!
        }
        
        refreshControl.addTarget(self, action: #selector(CatalogViewController.loadCategories), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        loadCategories()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return categories.count
        }
        else
        {
            return isSortingEnable ? sortedProducts!.count : products.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell0", for: indexPath)
            let category = categories[indexPath.row]
            
            cell.textLabel?.text = category.name
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! ProductCell
            let product = isSortingEnable ? sortedProducts![indexPath.row] : products[indexPath.row]
            
            cell.nameLabel.text = product.name
            cell.priceLabel.attributedText = product.attributedPrice
            cell.descriptionLabel.attributedText = product.attributedDescription
            
            if let urlString = product.pictureURL, let pictureURL = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            {
                cell.pictureImageView.kf.setImage(with: pictureURL)
                cell.pictureImageView.kf.indicatorType = .activity
            }
            else
            {
                cell.pictureImageView.image = nil
                cell.pictureImageView.kf.indicatorType = .activity
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.section == 0
        {
            let category = categories[indexPath.row]
            let catalogVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CatalogVC") as! CatalogViewController
            catalogVC.categoryName = category.name
            catalogVC.categoryID = category.iD
            navigationController?.pushViewController(catalogVC, animated: true)
        }
        else
        {
            performSegue(withIdentifier: "showProductSegue", sender: indexPath)
        }
    }
    
    // MARK: - filterViewControllerDelegate
    
    func minAvailablePrice() -> Int
    {
        return products.map{($0.actionPrice ?? $0.price)!}.min()!
    }
    
    func maxAvailablePrice() -> Int
    {
        return products.map{$0.price}.max()!
    }
    
    func availableBrands() -> [String]
    {
        return Array(Set(products.filter{$0.manufacturer != nil}.map{$0.manufacturer}))
    }
    
    func minSelectedPrice() -> Int
    {
        return minPriceForSorting ?? minAvailablePrice()
    }
    
    func maxSelectedPrice() -> Int
    {
        return maxPriceForSorting ?? maxAvailablePrice()
    }
    
    func isSelectedAscendingOrderSort() -> Bool
    {
        return inAscendingSortingOrder
    }
    
    func inSelectedDescendingOrderSort() -> Bool
    {
        return inDescendingSortingOrder
    }
    
    func selectedBrands() -> [String]
    {
        return brandsForSorting ?? []
    }
    
    func filterProducts(_ minPrice: Int, maxPrice: Int, brands: [String], inAscendingOrder: Bool, inDescendingOrder: Bool)
    {
        minPriceForSorting = minPrice
        maxPriceForSorting = maxPrice
        brandsForSorting = brands
        inAscendingSortingOrder = inAscendingOrder
        inDescendingSortingOrder = inDescendingOrder
        
        var sorted = products
            .filter{($0.actionPrice ?? $0.price)! >= minPrice}
            .filter{($0.actionPrice ?? $0.price)! <= maxPrice}
        
        if brands.count > 0
        {
            sorted = sorted.filter{$0.manufacturer != nil}.filter{brands.contains($0.manufacturer)}
        }
        
        if inAscendingOrder
        {
            sorted = sorted.sorted(by: {($0.actionPrice ?? $0.price)! < ($1.actionPrice ?? $1.price)!})
        }
        
        if inDescendingOrder
        {
            sorted = sorted.sorted(by: {($0.actionPrice ?? $0.price)! > ($1.actionPrice ?? $1.price)!})
        }
        
        isSortingEnable = true
        sortedProducts = sorted
        
        tableView.isHidden = sortedProducts?.count > 0 ? false : true
        tableView.reloadSections(IndexSet(integer: 1), with: .automatic)

    }
    
    // MARK: - Actions
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showProductSegue"
        {
            let indexPath = sender as! IndexPath
            let product = isSortingEnable ? sortedProducts![indexPath.row] : products[indexPath.row]
            
            let dvc = segue.destination as! ProductViewController
            dvc.productID = product.iD
            dvc.title = product.name
        }
        else if segue.identifier == "showFiltersSegue"
        {
            let dvc = segue.destination as! UINavigationController
            let fvc = dvc.topViewController as! FilterViewController
            
            fvc.delegate = self
        }
    }
    
    func searchButtonPressed(_ sender: AnyObject)
    {
        performSegue(withIdentifier: "showSearchSegue", sender: nil)
    }
    
    func filtersButtonPressed(_ sender: AnyObject)
    {
        if products.count > 0
        {
            performSegue(withIdentifier: "showFiltersSegue", sender: nil)
        }
    }
    
    func loadCategories()
    {
        ServiceAPI.sharedInstance.getCatalogCategories(categoryID) { [unowned self] (success: Bool, categories: [Category]) in
            
            self.refreshControl.endRefreshing()
            if success
            {
                self.categories = categories
                self.tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
            }
        }
        
        if let _ = categoryID
        {
            ServiceAPI.sharedInstance.getCategoryProducts(categoryID!) { [unowned self] (success: Bool, products: [Product]) in
                
                self.refreshControl.endRefreshing()
                if success
                {
                    self.products = products
                    self.tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
                }
            }
        }
    }
}
