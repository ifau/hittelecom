//
//  LeftMenuViewController.swift
//  hittelecom
//
//  Created by ifau on 20/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    fileprivate var selectedIndexPath = IndexPath(row: 0, section: 1)
    fileprivate let menuTitles = ["Каталог", "Оплата", "Доставка", "Контакты", "О компании"]
    fileprivate let segues = ["showCatalogSegue", "showPaymentSegue", "showDeliverySegue", "showContactSegue", "showAboutHittelecomSegue"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(LeftMenuViewController.receivedOpenCartViewControllerNotification(_:)), name: NSNotification.Name(rawValue: "needOpenCartViewControllerNotification"), object: nil)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    func receivedOpenCartViewControllerNotification(_ notification: Notification)
    {
        performSegue(withIdentifier: "showCartVC", sender: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section
        {
            case 0: return 1
            case 1: return menuTitles.count
            case 2: return 2
            default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 120
        }
        else if (indexPath.section == 2) && (indexPath.row == 0)
        {
            let height = UIScreen.main.bounds.size.height - 120 - (44 * 6)
            return height < 0 ? 10 : height
        }
        else
        {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            return tableView.dequeueReusableCell(withIdentifier: "Cell0", for: indexPath)
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
            cell.textLabel?.text = menuTitles[indexPath.row]
            if indexPath == selectedIndexPath
            {
                cell.backgroundColor = mainColor
                cell.textLabel?.textColor = UIColor.white
                cell.textLabel?.textAlignment = .natural
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            }
            else
            {
                cell.backgroundColor = UIColor.white
                cell.textLabel?.textColor = UIColor.black
                cell.textLabel?.textAlignment = .natural
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            }
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
            cell.textLabel?.text = indexPath.row == 1 ? "Разработано в Avium" : ""
            cell.backgroundColor = UIColor.white
            cell.textLabel?.textColor = UIColor.gray
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.section == 1
        {
            sideMenuController?.performSegue(withIdentifier: segues[indexPath.row], sender: nil)
            selectedIndexPath = indexPath
            tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        }
        else if (indexPath.section == 2) && (indexPath.row == 1)
        {
            self.performSegue(withIdentifier: "showAboutAviumSegue", sender: nil)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation)
    {
        tableView.reloadSections(IndexSet(integer: 2), with: .automatic)
    }
}
