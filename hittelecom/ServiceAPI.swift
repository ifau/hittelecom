//
//  ServiceAPI.swift
//  hittelecom
//
//  Created by ifau on 23/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import KVNProgress

class ServiceAPI: NSObject
{
    static let sharedInstance = ServiceAPI()
//    private let domain = "dev.hittele.com"
//    private let key = "ec89df5fb890b9612bff206d35e3fbdd"
    
    fileprivate let domain = "hittele.com"
    fileprivate let key = "02edc6fc7899e64ea4cf2c5dd48dda4d"
    
    fileprivate var requestsCount : Int = 0
    {
        didSet
        {
            UIApplication.shared.isNetworkActivityIndicatorVisible = requestsCount > 0 ? true : false
        }
    }
    
    fileprivate func sendGetRequest(_ urlString: String, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ())
    {
        let session = URLSession.shared
        
        let url = URL(string: "\(urlString)&Key=\(key)")!
        var request = URLRequest(url: url)
        
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        requestsCount = requestsCount + 1
        
        let task = session.dataTask(with: request, completionHandler: { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            self.requestsCount = self.requestsCount - 1
            DispatchQueue.main.async(execute: { completion(data, response, error) })
        }) 
        task.resume()
    }
    
    fileprivate func sendPostRequest(_ urlString: String, dataString: String, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ())
    {
        let session = URLSession.shared
        
        let url = URL(string: "\(urlString)&Key=\(key)")!
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.httpBody = dataString.data(using: String.Encoding.utf8)!
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        requestsCount = requestsCount + 1
        
        let task = session.dataTask(with: request, completionHandler: { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            self.requestsCount = self.requestsCount - 1
            DispatchQueue.main.async(execute: { completion(data, response, error) })
        }) 
        task.resume()
    }
    
    // MARK: - API Actions
    
    func getCatalogCategories(_ categoryID: String?, completion: @escaping (_ success: Bool, _ categories: [Category]) -> ())
    {
        let url = categoryID == nil ? "http://\(domain)/index.php?route=feed/web_api/category" : "http://\(domain)/index.php?route=feed/web_api/category&ID=\(categoryID!)"
        sendGetRequest(url)
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    let responseObject = CategoryResponseObject(fromDictionary: responseDictionary)
                    completion(responseObject.success, responseObject.categories)
                }
                catch
                {
                    completion(false, [])
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false, [])
            }
        }
    }
    
    func getCategoryProducts(_ categoryID: String, completion: @escaping (_ success: Bool, _ products: [Product]) -> ())
    {
        sendGetRequest("http://\(domain)/index.php?route=feed/web_api/products&Category=\(categoryID)")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    let responseObject = ProductsResponseObject(fromDictionary: responseDictionary)
                    completion(responseObject.success, responseObject.products)
                }
                catch
                {
                    completion(false, [])
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false, [])
            }
        }
    }
    
    func getProductInformation(_ productID: String, completion: @escaping (_ success: Bool, _ product: ProductFull?) -> ())
    {
        sendGetRequest("http://\(domain)/index.php?route=feed/web_api/product&ID=\(productID)")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    let responseObject = ProductResponseObject(fromDictionary: responseDictionary)
                    completion(responseObject.success, responseObject.product)
                }
                catch
                {
                    completion(false, nil)
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false, nil)
            }
        }
    }
    
    func getCostsAndDeliveryInformation(_ completion: @escaping (_ success: Bool, _ costs: Cost?) -> ())
    {
        sendGetRequest("http://\(domain)/index.php?route=feed/web_api/costs")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    let responseObject = CostsResponseObject(fromDictionary: responseDictionary)
                    completion((responseObject.costs != nil), responseObject.costs)
                }
                catch
                {
                    completion(false, nil)
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false, nil)
            }
        }
    }
    
    func sendOrder(_ orderDictionary: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool) -> ())
    {
        do
        {
            let data = try JSONSerialization.data(withJSONObject: orderDictionary, options: [])
            let jsonString = "data=\(String(data: data, encoding: String.Encoding.utf8)!)"
            let encodedJsonString = jsonString.replacingOccurrences(of: "&", with: "&amp;")
            
            sendPostRequest("https://\(domain)/index.php?route=feed/web_api/order", dataString: encodedJsonString, completion:
            { (data: Data?, response: URLResponse?, error: Error?) -> () in
                
                if error == nil
                {
                    do
                    {
                        let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                        if let success = responseDictionary.value(forKey: "Success") as? Bool, success == true, let id = responseDictionary.value(forKey: "ID") as? Int
                        {
                            KVNProgress.showSuccess(withStatus: "Заказ успешно отправлен. Номер заказа: \(id)")
                            completion(success)
                        }
                        else
                        {
                            KVNProgress.showError(withStatus: "Не удалось отправить заказ. Попробуйте еще раз.")
                            completion(false)
                        }
                    }
                    catch
                    {
                        KVNProgress.showError(withStatus: "Сервер временно недоступен. Попробуйте отправить заказ позднее.")
                        completion(false)
                    }
                }
                else
                {
                    KVNProgress.showError(withStatus: error!.localizedDescription)
                    completion(false)
                }
            })
        }
        catch
        {
            completion(false)
        }
    }
    
    func searchProducts(_ query: String, completion: @escaping (_ success: Bool, _ products: [Product]) -> ())
    {
        let encodedQuery = query.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        sendGetRequest("http://\(domain)/index.php?route=feed/web_api/search&Keyword=\(encodedQuery)")
        { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let responseDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    let responseObject = ProductsResponseObject(fromDictionary: responseDictionary)
                    completion(responseObject.success, responseObject.products)
                }
                catch
                {
                    completion(false, [])
                }
            }
            else
            {
                KVNProgress.showError(withStatus: error!.localizedDescription)
                completion(false, [])
            }
        }
    }

}
