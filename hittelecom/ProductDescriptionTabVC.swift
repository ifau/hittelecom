//
//  ProductDescriptionTabVC.swift
//  hittelecom
//
//  Created by ifau on 30/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import Kingfisher
import KVNProgress
import ImageViewer

class ProductDescriptionTabVC: UIViewController
{
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!

    var product: ProductFull!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        titleLabel.text = product.name
        priceLabel.attributedText = product.attributedPrice
        
        do
        {
            let data = product.descriptionField.data(using: String.Encoding.utf8)
            let attrStr = try NSAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
            descriptionLabel.attributedText = attrStr
        }
        catch
        {
            descriptionLabel.text = ""
        }
        
        if let urlString = product.pictureURL, let pictureURL = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
        {
            imageView.kf.setImage(with: pictureURL)
        }
        
        addToCartButton.backgroundColor = orangeColor
        addToCartButton.layer.cornerRadius = 4
        addToCartButton.clipsToBounds = true
        addToCartButton.setTitleColor(UIColor.white, for: UIControlState())
        
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTapped)))
    }
    
    @IBAction func addToCartButtonPressed(_ sender: AnyObject)
    {
        DataManager.sharedInstance.addProduct(product)
        KVNProgress.showSuccess(withStatus: "Товар добавлен")
    }
    
    func imageTapped(_ gestureRecogniser: UITapGestureRecognizer)
    {
        showGalleryImageViewer(gestureRecogniser.view!)
    }
    
    func showGalleryImageViewer(_ displacedView: UIView)
    {
        guard let cgImage = imageView.image?.cgImage else
        {
            return
        }
        
        guard UIApplication.shared.statusBarOrientation == .portrait else
        {
            return
        }
        
        let imageViewer = GalleryViewController(startIndex: 0, itemsDatasource: self, displacedViewsDatasource: nil, configuration: [GalleryConfigurationItem.thumbnailsButtonMode(.none)])
        self.presentImageGallery(imageViewer)
    }
    
    var imageCount: Int
    {
        return 1
    }
    
    func provideImage(_ completion: (UIImage?) -> Void)
    {
        completion(imageView.image)
    }
    
    func provideImage(atIndex index: Int, completion: (UIImage?) -> Void)
    {
        completion(imageView.image)
    }
}

extension ProductDescriptionTabVC: GalleryItemsDatasource
{
    func itemCount() -> Int
    {
        return 1
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem
    {
        let image = imageView.image
        return GalleryItem.image{ $0(image) }
    }
}
