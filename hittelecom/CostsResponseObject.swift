//
//  CostsResponseObject.swift
//  hittelecom
//
//  Created by ifau on 29/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class CostsResponseObject: NSObject
{
    var costs : Cost!
    
    init(fromDictionary dictionary: NSDictionary)
    {
        if let costsData = dictionary["Costs"] as? NSDictionary
        {
            costs = Cost(fromDictionary: costsData)
        }
    }
}

class Cost: NSObject
{
    var deliveries : [Delivery]!
    var payments : [Payment]!
    
    init(fromDictionary dictionary: NSDictionary)
    {
        deliveries = [Delivery]()
        if let deliveriesArray = dictionary["Deliveries"] as? [NSDictionary]
        {
            for dic in deliveriesArray{
                let value = Delivery(fromDictionary: dic)
                deliveries.append(value)
            }
        }
        payments = [Payment]()
        if let paymentsArray = dictionary["Payments"] as? [NSDictionary]
        {
            for dic in paymentsArray
            {
                let value = Payment(fromDictionary: dic)
                payments.append(value)
            }
        }
    }
}

class Payment: NSObject
{
    var commission : String!
    var iD : String!
    var name : String!
    var sortOrder : String!
    
    init(fromDictionary dictionary: NSDictionary)
    {
        commission = dictionary["Commission"] as? String
        iD = dictionary["ID"] as? String
        name = dictionary["Name"] as? String
        sortOrder = dictionary["SortOrder"] as? String
    }
    
    var attributedDescription: NSAttributedString
    {
        get
        {
            do
            {
                let data = name.data(using: String.Encoding.utf8)
                let attrStr = try NSAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
//                let muttableAttrStr = NSMutableAttributedString(attributedString: attrStr)
//                muttableAttrStr.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(14), range: NSRange(location: 0, length: attrStr.string.characters.count))
                
                return attrStr
            }
            catch
            {
                return NSAttributedString(string: "")
            }
        }
    }
    
    var attributedSelectedDescription: NSAttributedString
    {
        get
        {
            do
            {
                let data = name.data(using: String.Encoding.utf8)
                let attrStr = try NSAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
                let muttableAttrStr = NSMutableAttributedString(attributedString: attrStr)
                muttableAttrStr.addAttribute(NSForegroundColorAttributeName, value: orangeColor, range: NSRange(location: 0, length: attrStr.string.characters.count))
                
                return muttableAttrStr
            }
            catch
            {
                return NSAttributedString(string: "")
            }
        }
    }
}

class Delivery: NSObject
{    
    var cost : Int!
    var iD : String!
    var name : String!
    var sortOrder : String!
    
    init(fromDictionary dictionary: NSDictionary)
    {
        cost = dictionary["Cost"] as? Int
        iD = dictionary["ID"] as? String
        name = dictionary["Name"] as? String
        sortOrder = dictionary["SortOrder"] as? String
    }
    
    var attributedDescription: NSAttributedString
    {
        get
        {
            do
            {
                let data = name.data(using: String.Encoding.utf8)
                let attrStr = try NSAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
                return attrStr
            }
            catch
            {
                return NSAttributedString(string: "")
            }
        }
    }
    
    var attributedSelectedDescription: NSAttributedString
    {
        get
        {
            do
            {
                let data = name.data(using: String.Encoding.utf8)
                let attrStr = try NSAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
                let muttableAttrStr = NSMutableAttributedString(attributedString: attrStr)
                muttableAttrStr.addAttribute(NSForegroundColorAttributeName, value: orangeColor, range: NSRange(location: 0, length: attrStr.string.characters.count))
                
                return muttableAttrStr
            }
            catch
            {
                return NSAttributedString(string: "")
            }
        }
    }
}
