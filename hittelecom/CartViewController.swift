//
//  CartViewController.swift
//  hittelecom
//
//  Created by ifau on 01/07/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class CartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var orderButton: UIBarButtonItem!
    
    fileprivate var cartItems: [ProductStorageModel] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelectionDuringEditing = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        cartItems.removeAll()
        cartItems = DataManager.sharedInstance.getProducts()
        tableView.reloadData()
        
        orderButton.isEnabled = cartItems.count > 0 ? true : false
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        if DataManager.sharedInstance.managedObjectContext.hasChanges
        {
            do
            {
                try DataManager.sharedInstance.managedObjectContext.save()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "needUpdateCartBadgeNotification"), object: nil)
            }
            catch
            {
                
            }
        }
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return cartItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CartItemCell
        let cartItem = cartItems[indexPath.row]
        
        cell.nameLabel.text = cartItem.name!
        cell.priceLabel.text = "\(cartItem.price!.intValue) ₽"
        cell.quantityLabel.text = "Количество: \(cartItem.quantity!.intValue)"
        cell.totalPriceLabel.text = "Стоимость: \(cartItem.price!.intValue * cartItem.quantity!.intValue) ₽"
        
        cell.stepper.minimumValue = 1
        cell.stepper.maximumValue = 999
        cell.stepper.value = cartItem.quantity!.doubleValue
        cell.stepper.stepValue = 1
        cell.stepper.tag = indexPath.row
        cell.stepper.addTarget(self, action: #selector(CartViewController.stepperValueChanged(_:)), for: .valueChanged)
        
        if let urlString = cartItem.photo, let pictureURL = URL(string: urlString)
        {
            cell.pictureImageView.kf.setImage(with: pictureURL)
            cell.pictureImageView.kf.indicatorType = .activity
        }
        else
        {
            cell.pictureImageView.image = nil
            cell.pictureImageView.kf.indicatorType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        
        let footerLabel = UILabel()
        footerLabel.text = cartItems.count > 0 ? "Общая стоимость: \(cartItems.reduce(0) {$0 + $1.price!.intValue * $1.quantity!.intValue}) ₽"
            : "Корзина пуста"
        
        footerLabel.font = UIFont.systemFont(ofSize: 13)
        footerLabel.textColor = UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        footerLabel.textAlignment = NSTextAlignment.center
        return footerLabel
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 70
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            let cartItem = cartItems[indexPath.row]
            DataManager.sharedInstance.removeProduct(cartItem)
            cartItems.remove(at: indexPath.row)
            tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
            orderButton.isEnabled = cartItems.count > 0 ? true : false
        }
    }
    
    // MARK: - Actions
    
    @IBAction func closeButtonPressed(_ sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func orderButtonPressed(_ sender: AnyObject)
    {
        performSegue(withIdentifier: "showOrderVCSegue", sender: nil)
    }
    
    func stepperValueChanged(_ sender: AnyObject)
    {
        if let stepper = sender as? UIStepper
        {
            let cartItem = cartItems[stepper.tag]
            let contentOffset = tableView.contentOffset
            
            cartItem.quantity = NSNumber(value: stepper.value as Double)
            tableView.reloadData()
            tableView.layoutIfNeeded()
            tableView.contentOffset = contentOffset
        }
    }
}
