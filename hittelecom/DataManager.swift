//
//  DataManager.swift
//  proline
//
//  Created by ifau on 19/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import CoreData

class DataManager: NSObject
{
    static let sharedInstance = DataManager()
    var managedObjectContext: NSManagedObjectContext
    
    override init()
    {
        guard let modelURL = Bundle.main.url(forResource: "DataModel", withExtension:"momd") else
        {
            fatalError("Error loading model from bundle")
        }
        
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else
        {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        self.managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        self.managedObjectContext.persistentStoreCoordinator = psc
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docURL = urls[urls.endIndex - 1]
        

        let storeURL = docURL.appendingPathComponent("data.sqlite")
        
        do
        {
            try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: [NSMigratePersistentStoresAutomaticallyOption : true, NSInferMappingModelAutomaticallyOption : true])
        }
        catch
        {
            fatalError("Error migrating store: \(error)")
        }
    }
    
    func addProduct(_ product: ProductFull)
    {
        let id = product.iD
        let price = product.actionPrice == nil ? product.price : product.actionPrice!
        let name = product.name
        let photo = product.pictureURL
        
        do
        {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductEntity")
            request.predicate = NSPredicate(format: "id == %@", argumentArray: [product.iD])
            let fetchedProducts = try managedObjectContext.fetch(request) as! [ProductStorageModel]
            
            if fetchedProducts.count != 0
            {
                let entity = fetchedProducts.first!
                entity.quantity = NSNumber(value: entity.quantity!.intValue + 1 as Int)

                do
                {
                    try managedObjectContext.save()
                }
                catch
                {
                    fatalError("Failure to save context: \(error)")
                }
            }
            else
            {
                let entity = NSEntityDescription.insertNewObject(forEntityName: "ProductEntity", into: managedObjectContext) as! ProductStorageModel
                
                entity.id = id
                entity.price = NSNumber(value: price! as Int)
                entity.quantity = NSNumber(value: 1 as Int)
                entity.name = name
                entity.photo = photo
                
                do
                {
                    try managedObjectContext.save()
                }
                catch
                {
                    fatalError("Failure to save context: \(error)")
                }
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: "needUpdateCartBadgeNotification"), object: nil)
        }
        catch
        {
            fatalError("Failure to fetch entity: \(error)")
        }
    }

    func getProducts() -> [ProductStorageModel]
    {
        do
        {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductEntity")
            let fetchedProducts = try managedObjectContext.fetch(request) as! [ProductStorageModel]
            return fetchedProducts
        }
        catch
        {
            return []
        }
    }
    
    func getProductsCount() -> Int
    {
        do
        {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductEntity")
            let fetchedProducts = try managedObjectContext.fetch(request) as! [ProductStorageModel]
            var count = 0
            for product in fetchedProducts
            {
                count += product.quantity!.intValue
            }
            return count
        }
        catch
        {
            return 0
        }
    }
    
    func removeAllProducts()
    {
        do
        {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductEntity")
            let fetchedProducts = try managedObjectContext.fetch(request) as! [ProductStorageModel]
            for product in fetchedProducts
            {
                managedObjectContext.delete(product)
            }
            
            do
            {
                try managedObjectContext.save()
            }
            catch
            {
                fatalError("Failure to save context: \(error)")
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: "needUpdateCartBadgeNotification"), object: nil)
        }
        catch
        {
            fatalError("Failure to fetch entity: \(error)")
        }
    }
    
    func removeProduct(_ product: ProductStorageModel)
    {
        do
        {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductEntity")
            request.predicate = NSPredicate(format: "id == %@", argumentArray: [product.id!])
            let fetchedProducts = try managedObjectContext.fetch(request) as! [ProductStorageModel]
            
            if fetchedProducts.count != 0
            {
                for product in fetchedProducts
                {
                    managedObjectContext.delete(product)
                }
                do
                {
                    try managedObjectContext.save()
                }
                catch
                {
                    fatalError("Failure to save context: \(error)")
                }
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: "needUpdateCartBadgeNotification"), object: nil)
        }
        catch
        {
            fatalError("Failure to fetch entity: \(error)")
        }
    }
    
    func getClientInfo() -> ClientStorageModel?
    {
        do
        {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ClientEntity")
            let fetchedClients = try managedObjectContext.fetch(request) as! [ClientStorageModel]
            
            return fetchedClients.count > 0 ? fetchedClients.first! : nil
        }
        catch
        {
            fatalError("Failure to fetch entity: \(error)")
        }
    }
    
    func saveClientInfo(name: String, address: String, phone: String)
    {
        do
        {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ClientEntity")
            let fetchedClients = try managedObjectContext.fetch(request) as! [ClientStorageModel]
            
            if fetchedClients.count != 0
            {
                let client = fetchedClients.first!
                client.name = name
                client.address = address
                client.phone = phone
                
            }
            else
            {
                let client = NSEntityDescription.insertNewObject(forEntityName: "ClientEntity", into: managedObjectContext) as! ClientStorageModel
                
                client.name = name
                client.address = address
                client.phone = phone
            }
            
            do
            {
                try managedObjectContext.save()
            }
            catch
            {
                fatalError("Failure to save context: \(error)")
            }
        }
        catch
        {
            fatalError("Failure to fetch entity: \(error)")
        }
    }
}
