//
//  SearchViewController.swift
//  hittelecom
//
//  Created by ifau on 16/08/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    fileprivate var products: [Product] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        emptyView.isHidden = true
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        
        tableView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        searchController.isActive = true
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.async(execute: {
            self.searchController.searchBar.becomeFirstResponder()
        });
    }
    
    // MARK: - UITableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ProductCell
        let product = products[indexPath.row]
        
        cell.nameLabel.text = product.name
        cell.priceLabel.attributedText = product.attributedPrice
        cell.descriptionLabel.attributedText = product.attributedDescription
        
        if let urlString = product.pictureURL, let pictureURL = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
        {
            cell.pictureImageView.kf.setImage(with: pictureURL)
            cell.pictureImageView.kf.indicatorType = .activity
        }
        else
        {
            cell.pictureImageView.image = nil
            cell.pictureImageView.kf.indicatorType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        performSegue(withIdentifier: "showProductSegue", sender: indexPath)
    }
    
    // MARK: - UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController)
    {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        if let query = searchController.searchBar.text
        {
            searchProducts(query)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        emptyView.isHidden = true
    }
    
    // MARK: - Actions
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showProductSegue"
        {
            let indexPath = sender as! IndexPath
            let product = products[indexPath.row]
            
            let dvc = segue.destination as! ProductViewController
            dvc.productID = product.iD
            dvc.title = product.name
        }
    }

    func searchProducts(_ query: String)
    {
        ServiceAPI.sharedInstance.searchProducts(query) { [unowned self] (success: Bool, products: [Product]) in
        
            if success
            {
                self.products = products
                self.emptyView.isHidden = products.count > 0 ? true : false
                //self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)
                self.tableView.reloadData()
            }
        }
    }

}
