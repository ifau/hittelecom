//
//  ProductsResponseObject.swift
//  hittelecom
//
//  Created by ifau on 23/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class ProductsResponseObject: NSObject
{
    var products: [Product]!
    var success: Bool!

    init(fromDictionary dictionary: NSDictionary)
    {
        products = [Product]()
        if let productsArray = dictionary["Products"] as? [NSDictionary]
        {
            for dic in productsArray
            {
                let value = Product(fromDictionary: dic)
                products.append(value)
            }
        }
        success = dictionary["Success"] as? Bool
    }
}

class Product: NSObject
{
    var descriptionField: String!
    var iD: String!
    var name: String!
    var pictureURL: String?
    var price: Int!
    var actionPrice: Int?
    var manufacturer: String!
    
    init(fromDictionary dictionary: NSDictionary)
    {
        descriptionField = dictionary["Description"] as? String
        iD = dictionary["ID"] as? String
        name = dictionary["Name"] as? String
        pictureURL = dictionary["PictureURL"] as? String
        price = dictionary["Price"] as? Int
        actionPrice = dictionary["ActionPrice"] as? Int
        manufacturer = dictionary["Manufacturer"] as? String
    }
    
    var attributedDescription: NSAttributedString
    {
        get
        {
            do
            {
                let data = descriptionField.data(using: String.Encoding.utf8)
                let attrStr = try NSAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
                return attrStr
            }
            catch
            {
                return NSAttributedString(string: "")
            }
        }
    }
    
    var attributedPrice: NSAttributedString
    {
        get
        {
            let string = NSMutableAttributedString()
            
            let price_string = "\(price!)\(actionPrice == nil ? " ₽" : "")"
            
            let str = NSMutableAttributedString(string: price_string)
            str.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14), range: NSRange(location: 0, length: price_string.characters.count - 1))
            str.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: price_string.characters.count))
            
            if actionPrice != nil
            {
                str.addAttribute(NSStrikethroughStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: price_string.characters.count))
            }
            
            string.append(str)
            
            
            if actionPrice != nil
            {
                let price_string = " \(actionPrice!) ₽"
                
                let str = NSMutableAttributedString(string: price_string)
                str.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14), range: NSRange(location: 1, length: price_string.characters.count - 1))
                str.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0), range: NSRange(location: 1, length: price_string.characters.count - 1))
                
                string.append(str)
            }
            
            return string
        }
    }
}
