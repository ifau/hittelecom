//
//  CartBarButton.swift
//  hittelecom
//
//  Created by ifau on 15/07/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift

class CartBarButton: UIBarButtonItem
{
    var badgeButton: MIBadgeButton?
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize()
    {
        badgeButton = MIBadgeButton(type: .custom)
        badgeButton!.addTarget(self, action: #selector(CartBarButton.openCart), for: .touchUpInside)
        badgeButton!.setImage(UIImage(named: "IconCart"), for: .normal)
        badgeButton!.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        
        self.customView = badgeButton!
        updateBadge()

        NotificationCenter.default.addObserver(self, selector: #selector(CartBarButton.receivedUpdateBadgeNotification(_:)), name:NSNotification.Name(rawValue: "needUpdateCartBadgeNotification"), object: nil)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    func receivedUpdateBadgeNotification(_ notification: Notification)
    {
        updateBadge()
    }
    
    func updateBadge()
    {
        let value = DataManager.sharedInstance.getProductsCount()
        badgeButton?.badgeEdgeInsets = value > 0 ? UIEdgeInsetsMake(15, 0, 0, 5) : UIEdgeInsetsMake(15, 0, 200, 5)
        badgeButton?.badgeString = "\(value)"
    }
    
    func openCart()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "needOpenCartViewControllerNotification"), object: nil)
    }
}
