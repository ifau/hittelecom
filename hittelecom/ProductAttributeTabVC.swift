//
//  ProductAttributeTabVC.swift
//  hittelecom
//
//  Created by ifau on 01/07/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class ProductAttributeTabVC: UIViewController
{
    @IBOutlet weak var descriptionLabel: UILabel!
    var attributes: [AttributeValue]!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var string = ""
        
        for attribute in attributes
        {
            string += "\(attribute.name!)\n\(attribute.text!)\n\n"
        }
        
        let attrString = NSMutableAttributedString(string: string)
        attrString.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 12), range: NSRange(location: 0, length: string.characters.count))
        
        for attribute in attributes
        {
            attrString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 12), range: (string as NSString).range(of: attribute.name))
        }
        
        descriptionLabel.attributedText = attrString
    }
}
