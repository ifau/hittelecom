//
//  MakeRouteView.swift
//  actionshee
//
//  Created by ifau on 03/08/16.
//  Copyright © 2016 ifau. All rights reserved.
//

import UIKit

class MakeRouteView: UIView
{
    @IBOutlet var destinationLabel: UILabel!
    @IBOutlet var appsCollectionView: UICollectionView!
    
    fileprivate var appTitles = ["Apple\nMaps"]
    fileprivate var appIcons = ["IconAppleMaps"]
    fileprivate var links: [String] = []
    
    func setDestination(_ destination: String, latitude: String, longitude: String)
    {
        destinationLabel.text = destination
        destinationLabel.textColor = UIColor.lightGray
        
        links.append("http://maps.apple.com/?ll=\(latitude),\(longitude)&z=16")
        
        if UIApplication.shared.canOpenURL(URL(string: "yandexnavi://")!)
        {
            appTitles.append("Яндекс\nНавигатор")
            appIcons.append("IconYandexNavi")
            links.append("yandexnavi://build_route_on_map?lat_to=\(latitude)&lon_to=\(longitude)")
        }
        
        if UIApplication.shared.canOpenURL(URL(string: "yandexmaps://")!)
        {
            appTitles.append("Яндекс\nКарты")
            appIcons.append("IconYandexMaps")
            links.append("yandexmaps://build_route_on_map?lat_to=\(latitude)&lon_to=\(longitude)")
        }
        
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
        {
            appTitles.append("Google\nMaps")
            appIcons.append("IconGoogleMaps")
            links.append("comgooglemaps://?saddr=Current+Location&daddr=\(latitude),\(longitude)")
        }
    
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.minimumInteritemSpacing = 0.0
        layout.minimumLineSpacing = 0.0
        
        appsCollectionView.setCollectionViewLayout(layout, animated: false)
        appsCollectionView.register(RouteAppCell.self, forCellWithReuseIdentifier: "RouteAppCell")
        appsCollectionView.backgroundColor = UIColor.clear
        appsCollectionView.alwaysBounceHorizontal = true
        appsCollectionView.delegate = self
        appsCollectionView.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
}

extension MakeRouteView: UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RouteAppCell", for: indexPath) as! RouteAppCell
        
        cell.textLabel.text = appTitles[indexPath.row]
        cell.imageView.image = UIImage(named: appIcons[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 76, height: 100)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return appTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        if let url = URL(string: links[indexPath.row])
        {
            UIApplication.shared.openURL(url)
        }
    }
}

class RouteAppCell: UICollectionViewCell
{
    var textLabel : UILabel!
    var imageView : UIImageView!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        textLabel = UILabel()
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.font = UIFont.systemFont(ofSize: 12)
        textLabel.textColor = UIColor.black
        textLabel.numberOfLines = 0
        textLabel.textAlignment = .center
        self.contentView.addSubview(textLabel)
        
        imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(imageView)

        let views = ["textLabel" : textLabel, "imageView" : imageView]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[imageView(==60)]", options: .directionLeftToRight, metrics: nil, views: views))
        self.contentView.addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1, constant: 0))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[imageView(==60)]-4-[textLabel]-0-|", options: .directionLeftToRight, metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[textLabel]-0-|", options: .directionLeftToRight, metrics: nil, views: views))
    }
    
    convenience init()
    {
        self.init(frame:CGRect.zero)
    }
    
    required convenience init?(coder aDecoder: NSCoder)
    {
        self.init(frame:CGRect.zero)
    }
}
