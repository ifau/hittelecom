//
//  DeliveryRegionsCell.swift
//  hittelecom
//
//  Created by ifau on 24/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class DeliveryRegionsCell: UITableViewCell
{
    @IBOutlet var buttons: [UIButton]!
    var links = ["http://www.ae5000.ru/rates/calculate/", "http://www.ae5000.ru/senders/state_delivery/",
                 "http://www.jde.ru/calc", "http://cabinet.jde.ru/index.html",
                 "http://www.dellin.ru/calculator/", "http://www.dellin.ru/tracker/",
                 "http://www.pecom.ru/ru/calc/", "https://kabinet.pecom.ru/status/"]
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        for (index, button) in buttons.enumerated()
        {
            button.tag = index
            button.addTarget(self, action: #selector(DeliveryRegionsCell.buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    func buttonPressed(_ sender: UIButton)
    {
        if let url = URL(string: links[sender.tag])
        {
            UIApplication.shared.openURL(url)
        }
    }
}
