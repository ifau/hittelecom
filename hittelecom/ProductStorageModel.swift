//
//  ProductStorageModel.swift
//  proline
//
//  Created by ifau on 19/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import CoreData

class ProductStorageModel: NSManagedObject
{
    @NSManaged var id: String?
    @NSManaged var price: NSNumber?
    @NSManaged var quantity: NSNumber?
    @NSManaged var name: String?
    @NSManaged var photo: String?
}
