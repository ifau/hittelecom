//
//  OrderViewController.swift
//  hittelecom
//
//  Created by ifau on 29/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButton: UIBarButtonItem!
    
    let refreshControl = UIRefreshControl()
    
    var deliveries : [Delivery]!
    var payments : [Payment]!
    
    var selectedDeliveryIndexPath = IndexPath(row: 0, section: 1)
    var selectedPaymentIndexPath = IndexPath(row: 0, section: 2)
    
    fileprivate var name: String = ""
    fileprivate var phone: String = ""
    fileprivate var address: String = ""
    fileprivate var comment: String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        sendButton.isEnabled = false
        
        if let info = DataManager.sharedInstance.getClientInfo()
        {
            name = info.name!
            address = info.address!
            phone = info.phone!
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        
        refreshControl.addTarget(self, action: #selector(OrderViewController.loadCosts), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        loadCosts()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        //return ((deliveries == nil) && (payments == nil)) ? 1 : 3
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section
        {
            case 0: return 4
            case 1: return deliveries == nil ? 0 : deliveries.count
            case 2: return payments == nil ? 0 : payments.count
            default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section
        {
            case 1: return "Способ доставки"
            case 2: return "Способ оплаты"
            default: return nil
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! TextFieldCell
            
            if indexPath.row == 0
            {
                cell.titleLabel.text = "Имя*"
                cell.textField.text = name
                cell.textField.keyboardType = .default
            }
            else if indexPath.row == 1
            {
                cell.titleLabel.text = "Телефон*"
                cell.textField.text = phone
                cell.textField.keyboardType = .phonePad
            }
            else if indexPath.row == 2
            {
                cell.titleLabel.text = "Адрес доставки"
                cell.textField.text = address
                cell.textField.keyboardType = .default
            }
            else if indexPath.row == 3
            {
                cell.titleLabel.text = "Комментарий"
                cell.textField.text = comment
                cell.textField.keyboardType = .default
            }
            
            cell.textField.tag = indexPath.row
            cell.textField.addTarget(self, action:#selector(OrderViewController.textFieldDidChangeValue(_:)), for: UIControlEvents.editingChanged)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath)
            
            if indexPath.section == 1
            {
                let delivery = deliveries[indexPath.row]
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.attributedText = indexPath == selectedDeliveryIndexPath ? delivery.attributedSelectedDescription : delivery.attributedDescription
                cell.accessoryType = indexPath == selectedDeliveryIndexPath ? .checkmark : .none
            }
            else if indexPath.section == 2
            {
                let payment = payments[indexPath.row]
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.attributedText = indexPath == selectedPaymentIndexPath ? payment.attributedSelectedDescription : payment.attributedDescription
                cell.accessoryType = indexPath == selectedPaymentIndexPath ? .checkmark : .none
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if (indexPath.section == 1) && (indexPath != selectedDeliveryIndexPath)
        {
            let oldIndexPath = selectedDeliveryIndexPath
            selectedDeliveryIndexPath = indexPath
            
            tableView.reloadRows(at: [oldIndexPath, selectedDeliveryIndexPath], with: .automatic)
        }
        else if (indexPath.section == 2) && (indexPath != selectedPaymentIndexPath)
        {
            let oldIndexPath = selectedPaymentIndexPath
            selectedPaymentIndexPath = indexPath
            
            tableView.reloadRows(at: [oldIndexPath, selectedPaymentIndexPath], with: .automatic)
        }
    }
    
    // MARK: - Actions
    
    func textFieldDidChangeValue(_ sender: UITextField)
    {
        switch sender.tag
        {
            case 0: name = sender.text!
            case 1: phone = sender.text!
            case 2: address = sender.text!
            case 3: comment = sender.text!
            default: break
        }
        
        sendButton.isEnabled = ((name.characters.count > 1) && (phone.characters.count > 1) && (deliveries.count > 0) && (payments.count > 0))
        DataManager.sharedInstance.saveClientInfo(name: name, address: address, phone: phone)
    }
    
    func loadCosts()
    {
        ServiceAPI.sharedInstance.getCostsAndDeliveryInformation { [unowned self] (success: Bool, costs: Cost?) in
            
            self.refreshControl.endRefreshing()
            if success
            {
                self.payments = costs!.payments
                self.deliveries = costs!.deliveries
                
                let indexSet = NSMutableIndexSet()
                indexSet.add(1)
                indexSet.add(2)
                self.tableView.reloadSections(indexSet as IndexSet, with: .automatic)
            }
        }
    }
    
    @IBAction func sendButtonPressed(_ sender: AnyObject)
    {
        var dict = Dictionary<String,AnyObject>()
        dict["Name"] = name as AnyObject?
        dict["Phone"] = phone as AnyObject?
        dict["Address"] = address as AnyObject?
        dict["Comment"] = comment as AnyObject?
        dict["DeliveryID"] = deliveries[selectedDeliveryIndexPath.row].iD as AnyObject?
        dict["PaymentID"] = payments[selectedPaymentIndexPath.row].iD as AnyObject?
        
        var basket: [Dictionary<String, AnyObject>] = []
        let products = DataManager.sharedInstance.getProducts()
        for product in products
        {
            basket.append(["ID":Int(product.id!) as AnyObject, "Count":product.quantity!])
        }
        dict["Basket"] = basket as AnyObject?
        
        ServiceAPI.sharedInstance.sendOrder(dict) { [unowned self] (success: Bool) in
            
            if success
            {
                DataManager.sharedInstance.removeAllProducts()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
