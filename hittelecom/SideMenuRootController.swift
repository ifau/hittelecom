//
//  SideMenuRootController.swift
//  hittelecom
//
//  Created by ifau on 27/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import SideMenuController

class SideMenuRootController: SideMenuController, SideMenuControllerDelegate
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        performSegue(withIdentifier: "showCatalogSegue", sender: nil)
        performSegue(withIdentifier: "LeftMenuSegue", sender: nil)
        
        sideMenuController?.delegate = self
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.sideMenuController = self
    }
    
    func sideMenuControllerDidHide(_ sideMenuController: SideMenuController)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.canChangeOrientation = true
    }
    
    func sideMenuControllerDidReveal(_ sideMenuController: SideMenuController)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.canChangeOrientation = false
    }
}
