//
//  RangeSliderCell.swift
//  hittelecom
//
//  Created by ifau on 17/08/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import TTRangeSlider

class RangeSliderCell: UITableViewCell
{
    @IBOutlet var rangeSlider: TTRangeSlider!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        rangeSlider.handleColor = orangeColor
        rangeSlider.tintColorBetweenHandles = orangeColor
        rangeSlider.maxLabelColour = orangeColor
        rangeSlider.minLabelColour = orangeColor
        rangeSlider.tintColor = orangeColor
        rangeSlider.lineHeight = 1
    }
}
