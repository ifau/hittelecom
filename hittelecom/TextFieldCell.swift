//
//  TextFieldCell.swift
//  hittelecom
//
//  Created by ifau on 30/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell
{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
}
