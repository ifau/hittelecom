//
//  ClientStorageModel.swift
//  proline
//
//  Created by ifau on 26/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import CoreData

class ClientStorageModel: NSManagedObject
{
    @NSManaged var name: String?
    @NSManaged var address: String?
    @NSManaged var phone: String?
}
