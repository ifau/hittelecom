//
//  ProductResponseObject.swift
//  hittelecom
//
//  Created by ifau on 30/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class ProductResponseObject: NSObject
{
    var product : ProductFull!
    var success : Bool!
    
    init(fromDictionary dictionary: NSDictionary)
    {
        if let productData = dictionary["Product"] as? NSDictionary
        {
            product = ProductFull(fromDictionary: productData)
        }
        success = dictionary["Success"] as? Bool
    }
}

class ProductFull: NSObject
{
    var additionalTabs : [AdditionalTab]!
    var attributes : [Attribute]!
    var descriptionField : String!
    var iD : String!
    var name : String!
    var pictureURL : String?
    var price : Int!
    var actionPrice: Int?
    
    init(fromDictionary dictionary: NSDictionary)
    {
        additionalTabs = [AdditionalTab]()
        if let additionalTabsArray = dictionary["AdditionalTabs"] as? [NSDictionary]
        {
            for dic in additionalTabsArray
            {
                let value = AdditionalTab(fromDictionary: dic)
                additionalTabs.append(value)
            }
        }
        attributes = [Attribute]()
        if let attributesArray = dictionary["Attributes"] as? [NSDictionary]
        {
            for dic in attributesArray
            {
                let value = Attribute(fromDictionary: dic)
                attributes.append(value)
            }
        }
        descriptionField = dictionary["Description"] as? String
        iD = dictionary["ID"] as? String
        name = dictionary["Name"] as? String
        pictureURL = dictionary["PictureURL"] as? String
        price = dictionary["Price"] as? Int
        actionPrice = dictionary["ActionPrice"] as? Int
    }
    
    var attributedPrice: NSAttributedString
    {
        get
        {
            let string = NSMutableAttributedString()
            
            let price_string = "\(price!)\(actionPrice == nil ? " ₽" : "")"
            
            let str = NSMutableAttributedString(string: price_string)
            str.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 14), range: NSRange(location: 0, length: price_string.characters.count - 1))
            str.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: price_string.characters.count))
            
            if actionPrice != nil
            {
                str.addAttribute(NSStrikethroughStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: price_string.characters.count))
            }
            
            string.append(str)
            
            
            if actionPrice != nil
            {
                let price_string = " \(actionPrice!) ₽"
                
                let str = NSMutableAttributedString(string: price_string)
                str.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 14), range: NSRange(location: 1, length: price_string.characters.count - 1))
                str.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0), range: NSRange(location: 1, length: price_string.characters.count - 1))
                
                string.append(str)
            }
            
            return string
        }
    }

}

class AdditionalTab: NSObject
{
    var compare : String!
    var content : String!
    var href : String!
    var position : String!
    var title : String!
    
    init(fromDictionary dictionary: NSDictionary)
    {
        compare = dictionary["compare"] as? String
        content = dictionary["content"] as? String
        href = dictionary["href"] as? String
        position = dictionary["position"] as? String
        title = dictionary["title"] as? String
    }
}

class Attribute: NSObject
{
    var name : String!
    var attributeGroupId : String!
    var attribute : [AttributeValue]!
    
    init(fromDictionary dictionary: NSDictionary)
    {
        name = dictionary["name"] as? String
        attribute = [AttributeValue]()
        if let attributeArray = dictionary["attribute"] as? [NSDictionary]
        {
            for dic in attributeArray
            {
                let value = AttributeValue(fromDictionary: dic)
                attribute.append(value)
            }
        }
        attributeGroupId = dictionary["attribute_group_id"] as? String
    }
}

class AttributeValue: NSObject
{
    var attributeId : String!
    var name : String!
    var text : String!

    init(fromDictionary dictionary: NSDictionary)
    {
        attributeId = dictionary["attribute_id"] as? String
        name = dictionary["name"] as? String
        text = dictionary["text"] as? String
    }
}

