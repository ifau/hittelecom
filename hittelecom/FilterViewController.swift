//
//  FilterViewController.swift
//  hittelecom
//
//  Created by ifau on 17/08/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import TTRangeSlider

protocol filterViewControllerDelegate: class
{
    func minAvailablePrice() -> Int
    func maxAvailablePrice() -> Int
    func availableBrands() -> [String]
    
    func minSelectedPrice() -> Int
    func maxSelectedPrice() -> Int
    func isSelectedAscendingOrderSort() -> Bool
    func inSelectedDescendingOrderSort() -> Bool
    func selectedBrands() -> [String]
    
    func filterProducts(_ minPrice: Int, maxPrice: Int, brands: [String], inAscendingOrder: Bool, inDescendingOrder: Bool)
}

class FilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, TTRangeSliderDelegate
{
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate:filterViewControllerDelegate?
    fileprivate var brands: [String] = []
    fileprivate var sortIndexPath: IndexPath?
    fileprivate var brandsIndexPaths: [IndexPath] = []
    fileprivate var minPrice: Int!
    fileprivate var maxPrice: Int!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        brands = delegate!.availableBrands()
        minPrice = delegate!.minSelectedPrice()
        maxPrice = delegate!.maxSelectedPrice()
        
        if delegate!.isSelectedAscendingOrderSort()
        {
            sortIndexPath = IndexPath(row: 0, section: 1)
        }
        else if delegate!.inSelectedDescendingOrderSort()
        {
            sortIndexPath = IndexPath(row: 1, section: 1)
        }
        
        let selected = delegate!.selectedBrands()
        for brand in selected
        {
            if let index = brands.index(of: brand)
            {
                brandsIndexPaths.append(IndexPath(row: index, section: 2))
            }
        }
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - UITableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section
        {
            case 0: return 1
            case 1: return 2
            case 2: return brands.count
            default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section
        {
            case 0: return "Цена"
            case 1: return "Сортировать"
            case 2: return "Производители"
            default: return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return indexPath.section == 0 ? 70 : 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = indexPath.section == 0 ? "Cell0" : "Cell1"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        if indexPath.section == 0
        {
            cell.textLabel?.text = ""
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            let _cell = cell as! RangeSliderCell
            
            _cell.rangeSlider.delegate = self
            _cell.rangeSlider.minValue = Float(delegate!.minAvailablePrice())
            _cell.rangeSlider.maxValue = Float(delegate!.maxAvailablePrice())
            _cell.rangeSlider.selectedMinimum = Float(minPrice)
            _cell.rangeSlider.selectedMaximum = Float(maxPrice)
        }
        else if indexPath.section == 1
        {
            let labels = ["По возрастанию цены", "По убыванию цены"]
            cell.textLabel?.text = labels[indexPath.row]
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            
            if indexPath == sortIndexPath
            {
                cell.textLabel?.textColor = orangeColor
                cell.tintColor = orangeColor
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.textLabel?.textColor = UIColor.black
                cell.tintColor = UIColor.black
                cell.accessoryType = .none
            }
        }
        else if indexPath.section == 2
        {
            cell.textLabel?.text = brands[indexPath.row]
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            
            if brandsIndexPaths.contains(indexPath)
            {
                cell.textLabel?.textColor = orangeColor
                cell.tintColor = orangeColor
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.textLabel?.textColor = UIColor.black
                cell.tintColor = UIColor.black
                cell.accessoryType = .none
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1
        {
            sortIndexPath = sortIndexPath == indexPath ? nil : indexPath
            tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        }
        else if indexPath.section == 2
        {
            if let index = brandsIndexPaths.index(of: indexPath)
            {
                brandsIndexPaths.remove(at: index)
            }
            else
            {
                brandsIndexPaths.append(indexPath)
            }
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    // MARK: - TTRangeSliderDelegate
    
    func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float)
    {
        minPrice = Int(selectedMinimum)
        maxPrice = Int(selectedMaximum)
    }
    
    // MARK: - Actions
    
    func performSort()
    {
        var selectedBrands: [String] = []
        let ascendingOrder: Bool = ((sortIndexPath != nil) && (sortIndexPath!.row == 0))
        let descendingOrder: Bool = ((sortIndexPath != nil) && (sortIndexPath!.row == 1))
        
        for indexPath in brandsIndexPaths
        {
            selectedBrands.append(brands[indexPath.row])
        }
        
        delegate!.filterProducts(minPrice, maxPrice: maxPrice, brands: selectedBrands, inAscendingOrder: ascendingOrder, inDescendingOrder: descendingOrder)
    }
    
    @IBAction func closeButtonPressed(_ sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonPressed(_ sender: AnyObject)
    {
        performSort()
        self.dismiss(animated: true, completion: nil)
    }
}
