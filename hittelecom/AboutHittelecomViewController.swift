//
//  AboutHittelecomViewController.swift
//  hittelecom
//
//  Created by ifau on 31/08/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class AboutHittelecomViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var isExpanded: Bool = false
    fileprivate var expandedSection: Int = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "AboutHittelecomDescriptionCell", bundle: nil), forCellReuseIdentifier: "AboutHittelecomDescriptionCell")
        tableView.register(UINib(nibName: "AboutHittelecomFillsCell", bundle: nil), forCellReuseIdentifier: "AboutHittelecomFillsCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == expandedSection) && isExpanded
        {
            return 2
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            cell.textLabel?.text = indexPath.section == 0 ? "О нас" : "Реквизиты"
            cell.textLabel?.textColor = UIColor(red: 0, green: 100/255.0, blue: 161/255.0, alpha: 1.0)
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            return cell
        }
        else
        {
            let identifier = indexPath.section == 0 ? "AboutHittelecomDescriptionCell" : "AboutHittelecomFillsCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.row == 0
        {
            if indexPath.section == expandedSection
            {
                let indexSet = IndexSet(integer: indexPath.section)
                
                isExpanded = !isExpanded
                tableView.reloadSections(indexSet, with: .automatic)
            }
            else
            {
                let indexSet = NSMutableIndexSet()
                indexSet.add(expandedSection)
                indexSet.add(indexPath.section)
                
                isExpanded = true
                expandedSection = indexPath.section
                tableView.reloadSections(indexSet as IndexSet, with: .automatic)
            }
        }
    }
}
