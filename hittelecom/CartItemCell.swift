//
//  CartItemCell.swift
//  hittelecom
//
//  Created by ifau on 01/07/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class CartItemCell: UITableViewCell
{
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var quantityLabel: UILabel!
    @IBOutlet var totalPriceLabel: UILabel!
    @IBOutlet var pictureImageView: UIImageView!
    @IBOutlet var stepper: UIStepper!
}
