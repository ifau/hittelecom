//
//  ContactsViewController.swift
//  hittelecom
//
//  Created by ifau on 19/07/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import CustomizableActionSheet

class ContactsViewController: UIViewController, UITextViewDelegate
{
    @IBOutlet weak var descriptionTextView: UITextView!
    var actionSheet: CustomizableActionSheet?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let attributedDescription = NSMutableAttributedString(attributedString: descriptionTextView.attributedText)
        attributedDescription.addAttribute(NSLinkAttributeName, value: URL(string: "http://maps.apple.com/?ll=55.627806,37.617410&z=16")!, range: (descriptionTextView.attributedText.string as NSString).range(of: "117587, г Москва, Варшавское шоссе , д 125, стр 1, секция 9, 2 этаж, офис 207"))
        
        descriptionTextView.attributedText = attributedDescription
        descriptionTextView.delegate = self
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        if URL.absoluteString == "http://maps.apple.com/?ll=55.627806,37.617410&z=16"
        {
            var items = [CustomizableActionSheetItem]()

            if let routeAppsView = UINib(nibName: "MakeRouteView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? MakeRouteView
            {
                let viewItem = CustomizableActionSheetItem()
                viewItem.type = .view
                viewItem.view = routeAppsView
                viewItem.height = 190
                items.append(viewItem)
                
                routeAppsView.setDestination("Варшавское шоссе, д. 125, стр. 1", latitude: "55.627806", longitude: "37.61741")
            }
            
            let closeItem = CustomizableActionSheetItem()
            closeItem.type = .button
            closeItem.label = "Закрыть"
            closeItem.textColor = UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
            closeItem.selectAction = { (actionSheet: CustomizableActionSheet) -> Void in
                actionSheet.dismiss()
            }
            items.append(closeItem)
            
            let actionSheet = CustomizableActionSheet()
            self.actionSheet = actionSheet
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.canChangeOrientation = false
            actionSheet.showInView(appDelegate.sideMenuController!.view, items: items, closeBlock: { appDelegate.canChangeOrientation = true })
            
            return false
        }
        else
        {
            return true
        }
    }
}
