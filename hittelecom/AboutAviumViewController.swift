//
//  AboutAviumViewController.swift
//  hittelecom
//
//  Created by ifau on 05/07/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import CustomizableActionSheet

class AboutAviumViewController: UIViewController
{
    var actionSheet: CustomizableActionSheet?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func closeButtonPressed(_ sender: AnyObject)
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func webButtonPressed(_ sender: AnyObject)
    {
        UIApplication.shared.openURL(URL(string: "http://avium.ru")!)
    }
    
    @IBAction func mailButtonPressed(_ sender: AnyObject)
    {
        UIApplication.shared.openURL(URL(string: "mailto:info@avium.ru")!)
    }
    
    @IBAction func locationButtonPressed(_ sender: AnyObject)
    {
//        UIApplication.sharedApplication().openURL(NSURL(string: "http://maps.apple.com/?ll=55.937982,37.862013&z=16")!)
        
        var items = [CustomizableActionSheetItem]()
        
        if let routeAppsView = UINib(nibName: "MakeRouteView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? MakeRouteView
        {
            let viewItem = CustomizableActionSheetItem()
            viewItem.type = .view
            viewItem.view = routeAppsView
            viewItem.height = 190
            items.append(viewItem)
            
            routeAppsView.setDestination("г. Королев, ул. Московская, дом 3", latitude: "55.937982", longitude: "37.862013")
        }
        
        let closeItem = CustomizableActionSheetItem()
        closeItem.type = .button
        closeItem.label = "Закрыть"
        closeItem.textColor = UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
        closeItem.selectAction = { (actionSheet: CustomizableActionSheet) -> Void in
            actionSheet.dismiss()
        }
        items.append(closeItem)
        
        let actionSheet = CustomizableActionSheet()
        self.actionSheet = actionSheet
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.canChangeOrientation = false
        actionSheet.showInView(self.view, items: items, closeBlock: { appDelegate.canChangeOrientation = true })
    }
}
