//
//  AppDelegate.swift
//  hittelecom
//
//  Created by ifau on 20/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import SideMenuController
import KVNProgress
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    var canChangeOrientation = true
    var sideMenuController: SideMenuController?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "IconMenu")
        SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelLeft
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.drawing.sidePanelWidth = 240
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.statusBarBehaviour = .horizontalPan
        
        let config = KVNProgressConfiguration()
        config.isFullScreen = false
        config.minimumDisplayTime = 1.0
        config.minimumSuccessDisplayTime = 0.5
        config.minimumErrorDisplayTime = 3.0
        config.circleStrokeForegroundColor = orangeColor
        config.successColor = orangeColor
        config.errorColor = orangeColor
        config.statusColor = orangeColor
        //config.backgroundFillColor = mainColor
        KVNProgress.setConfiguration(config)
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
        if canChangeOrientation
        {
            return .allButUpsideDown
        }
        else
        {
            let currentOrientation = UIApplication.shared.statusBarOrientation
            switch currentOrientation
            {
                case .landscapeLeft: return .landscapeLeft
                case .landscapeRight: return .landscapeRight
                case .portrait: return .portrait
                default: return .allButUpsideDown
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

class initialViewController: UIViewController
{
    // becouse sidemenu cant be initial (bug?)
    
    override func viewDidAppear(_ animated: Bool)
    {
        performSegue(withIdentifier: "initialSegue", sender: nil)
    }
}

let mainColor = UIColor(red: 3.0/255.0, green: 96.0/255.0, blue: 174.0/255.0, alpha: 1.0)
let orangeColor = UIColor(red: 240.0/255.0, green: 88.0/255.0, blue: 52.0/255.0, alpha: 1.0)

