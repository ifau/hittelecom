//
//  ProductViewController.swift
//  hittelecom
//
//  Created by ifau on 30/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import PageMenu

class ProductViewController: UIViewController
{
    var productID: String!
    var pageMenu: CAPSPageMenu?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadProduct()
    }
    
    func initializePage(_ product: ProductFull)
    {
        var vcs: [UIViewController] = []
        
        let descriptionVC = ProductDescriptionTabVC(nibName: "ProductDescriptionTabVC", bundle: nil)
        descriptionVC.title = "Описание"
        descriptionVC.product = product
        vcs.append(descriptionVC)
        
        for tab in product.additionalTabs
        {
            let vc = ProductAdditionalTabVC(nibName: "ProductAdditionalTabVC", bundle: nil)
            vc.title = tab.title
            vc.descr = tab.content
            vcs.append(vc)
        }
        
        for attr in product.attributes
        {
            let vc = ProductAttributeTabVC(nibName: "ProductAttributeTabVC", bundle: nil)
            vc.title = attr.name
            vc.attributes = attr.attribute
            vcs.append(vc)
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .selectionIndicatorColor(orangeColor),
            .bottomMenuHairlineColor(orangeColor),
            .selectedMenuItemLabelColor(orangeColor),
            .unselectedMenuItemLabelColor(UIColor.black),
            .menuHeight(40.0),
            .centerMenuItems(true)
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: vcs, frame: CGRect(x: 0.0, y: self.topLayoutGuide.length, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        pageMenu!.view.alpha = 0
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
        
        UIView.animate(withDuration: 0.3)
        {
            self.pageMenu!.view.alpha = 1.0
        }
    }
    
    // MARK: - Actions
    
    func loadProduct()
    {
        ServiceAPI.sharedInstance.getProductInformation(productID) { [unowned self] (success: Bool, product: ProductFull?) in
            
            if success && (product != nil)
            {
                self.initializePage(product!)
            }
        }
    }
}
