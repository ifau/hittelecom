//
//  ProductAdditionalTabVC.swift
//  hittelecom
//
//  Created by ifau on 30/06/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class ProductAdditionalTabVC: UIViewController
{
    @IBOutlet weak var descriptionLabel: UILabel!
    var descr: String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if descr.contains("<iframe")
        {
            descriptionLabel.alpha = 0
            
            let webView = UIWebView()
            webView.translatesAutoresizingMaskIntoConstraints = false
            let responsivePage = "<style media=\"screen\" type=\"text/css\"> iframe {min-width: 100%; width: 100px; *width: 100%; }</style>\(descr)"
            self.view.addSubview(webView)
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[webView]-0-|", options: .directionLeftToRight, metrics: nil, views: ["webView" : webView]))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[webView]-0-|", options: .directionLeftToRight, metrics: nil, views: ["webView" : webView]))
            webView.loadHTMLString(responsivePage, baseURL: nil)
        }
        else
        {
            do
            {
                let data = descr.data(using: String.Encoding.utf8)
                let attrStr = try NSAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
                descriptionLabel.attributedText = attrStr
            }
            catch
            {
                descriptionLabel.text = ""
            }
        }
    }
}
